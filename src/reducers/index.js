import { combineReducers } from 'redux';

import works from './works_reducer';
import workItem from './work_item';

const rootReducer = combineReducers({
  works,
  workItem
});

export default rootReducer;
