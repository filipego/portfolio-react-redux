export default function(state = null, action) {
  switch(action.type) {
    case 'WORKS_LIST':
      return action.payload;
    default:
      return state;
  }
}
