export default function(state = null, action) {
  switch(action.type) {
    case 'WORK_ITEM':
      return action.payload;
    default:
      return state;
  }
}
