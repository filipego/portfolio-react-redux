import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { worksList } from '../actions';
import { bindActionCreators } from 'redux';


class Portfolio extends Component {

  componentWillMount() {
    this.props.worksList();
  }

  renderList = (works) => {
    if(works) {
      return works.map((work) => {
        return (
          <li key={work.id} className={`item item-${work.id}`}>
            <Link to={`/work/${work.id}`}>
              <img src={work.thumb} alt={work.title} />
              <span className="item-info">
                <span className="content">
                  <div className="title">{work.title}</div>
                  <div className="type-of-industry">{work.industry}</div>
                </span>
              </span>
            </Link>
          </li>
        );
      });
    }
  }

  render() {
    return (
      <div id="work" className="work">
        <ul className="grid">{this.renderList(this.props.works)}</ul>
      </div>
    );
  }

}

function mapStateToProps(state) {
  return {
    works: state.works
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    worksList
  }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(Portfolio);
