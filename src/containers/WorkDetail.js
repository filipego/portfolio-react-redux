import React, { Component } from 'react';
import HeaderDetail from '../components/common/HeaderDetail';
import { connect } from 'react-redux';
import { workDetail } from '../actions';
import { bindActionCreators } from 'redux';

class WorkDetail extends Component {

  componentDidMount() {
    this.props.workDetail(this.props.match.params.id -1);
  }

  renderWorkDetail = ( workItem ) => {
    if(workItem) {
      return (
        <div className="work-page">
          <div className="headline">
            <div className="container">
              <div className="headline-container">
                <h2>{workItem.title}</h2>
                <div>
                  <p>{workItem.description[0]}</p>
                  <p>{workItem.description[1]}</p>
                  <p>{workItem.description[2]}</p>
                </div>
                <span>{workItem.industry}</span>
                <span>
                  - <a href={workItem.urLLink} target="_blank">{workItem.urlName}</a>
                </span>
              </div>
            </div>
          </div>
          <div className="work-content">
            <div className="container">
              <div className="fields">
                <img src={workItem.img} alt={workItem.title} />
              </div>
            </div>
          </div>
        </div>
      );
    }
  }

  render() {
    return (
      <div>
        <HeaderDetail />
        {this.renderWorkDetail(this.props.workItem)}
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    workItem: state.workItem
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    workDetail
  }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(WorkDetail);
