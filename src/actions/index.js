import Data from '../Data.json'

export function worksList() {
  return {
    type: "WORKS_LIST",
    payload: Data
  }
}

export function workDetail(id) {
  return {
    type: "WORK_ITEM",
    payload: Data[id]
  }
}
