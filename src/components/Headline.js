import React from 'react';

const Headline = (props) => {
  return (
    <div id="headline" className="headline">
			<div className="container">
				<div className="headline-container">
					<h2>I'm Filipe a Front End Developer</h2>
					<p>I'm creative, reliable and professional.<br/>I'm available to work in North Jersey and Manhattan.</p>
				</div>
			</div>
    </div>
  );
};

export default Headline;
