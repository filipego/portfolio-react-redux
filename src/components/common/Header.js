import React from 'react';
import Scrollchor from 'react-scrollchor';

const Header = (props) => {
  return (
    <header id="header" className="header">
      <div className="container">
				<a href="/" className="logo">Filipe Valente</a>
				<nav>
					<ul>
						<li><Scrollchor to="work">work</Scrollchor></li>
						<li><Scrollchor to="about">about</Scrollchor></li>
						<li><Scrollchor to="skills">skills</Scrollchor></li>
						<li><Scrollchor to="contact">contact</Scrollchor></li>
					</ul>
				</nav>
			</div>
    </header>
  );
};

export default Header;
