import React from 'react';

const Footer = (props) => {
  return (
    <footer id="contact" className="site-footer">
			<div className="container">

				<h2>Let's connect</h2>
        
				<ul className="social">
					<li><a href="https://www.linkedin.com/in/filipevalentegomes" target="_blank" rel="noopener noreferrer"><i className="fa fa-linkedin" aria-hidden="true"></i></a></li>
					<li><a href="mailto:info@filipevalentegomes.com"><i className="fa fa-envelope-o" aria-hidden="true"></i></a></li>
					<li><a href="./misc/FilipeResume2017.docx"><i className="fa fa-download" aria-hidden="true"></i> Resume</a></li>
				</ul>
				<div className="copy">
					Copyright © 2017 Crafted by Filipe Valente.
				</div>
			</div>
		</footer>
  );
};

export default Footer;
