import React from 'react';
import Scrollchor from 'react-scrollchor';

const Header = (props) => {
  return (
    <header id="header" className="header">
      <div className="container">
				<a href="/" className="logo">Filipe Valente</a>
				<nav>
					<ul>
						<li><a href="/#work">work</a></li>
						<li><a href="/#about">about</a></li>
						<li><a href="/#skills">skills</a></li>
						<li><Scrollchor to="contact">contact</Scrollchor></li>
					</ul>
				</nav>
			</div>
    </header>
  );
};

export default Header;
