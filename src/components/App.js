import React, { Component } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import ScrollToTop from './ScrollToTop';

import NotFound from './NotFound';
import Footer from './common/Footer';
import Home from './Home';
import WorkDetail from '../containers/WorkDetail';


class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <ScrollToTop>
        <div id="main" className="site-main">
          <Switch>
            <Route path="/work/:id" component={WorkDetail} />
            <Route exact path="/" component={Home} />
            <Route path="*" component={NotFound}/>
          </Switch>
          <Footer />
        </div>
        </ScrollToTop>
      </BrowserRouter>
    );
  }
}

export default App;
