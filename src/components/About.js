import React from 'react';

const About = (props) => {
  return (
    <div id="about" className="about">
			<div className="container">
				<div className="about-content">
					<h2>About Me</h2>

					<p>My name is Filipe Valente and I've been a Front End Developer for over 12 years. I am of Portuguese decent born in Luxembourg (no, it is not a city in Germany, it is actually a very small, but very rich country right in the middle of Europe). I moved to the United States in 2005 and rest is history.</p>

					<p>I am very passionate about my work, and will make a case for what I believe in especially if it's for the benefit of my clients. My coding style is clean and efficient. I absolutely love what I do.</p>

					<p>I am what you call a "metrosexual geek". I love technology. I like to keep up to date with the latest and greatest of the industry. However, on any given Sunday you can find me watching a documentary. Learning is growing and I am constantly doing so.</p>
				</div>
			</div>
		</div>
  )
};

export default About;
