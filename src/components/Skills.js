import React from 'react';

const Skills = (props) => {
  return (
    <div id="skills" className="skills">
				<div className="container">
					<h2>Skills</h2>
					<ul>
						<li className="col">
							<span>
								<i className="fa fa-code" aria-hidden="true"></i>
							</span>
							<h3>Development</h3>
							<p>HTML, CSS, SCSS, JavaScript, jQuery, React.js, React-Native, Vue.js(The basics), Node.js(The basics), Webpack, Responsive Web Design, Bootstraps, WordPress, HTML Emails, Git, Wireframes, Browser Standards, ...</p>
						</li>
						<li className="col">
							<span>
								<i className="fa fa-laptop" aria-hidden="true"></i>
							</span>
							<h3>Software</h3>
							<p>Mac and Windows, MS Office, Adobe CC, Atom, Sublime 3, Terminal, LAMP and MAMP, Spring, jDeveloper, Visual Studio, FileZila</p>
						</li>
						<li className="col">
							<span>
								<i className="fa fa-language" aria-hidden="true"></i>
							</span>
							<h3>Languages</h3>
							<p>Here are the 6 languages I speak: Native French, Fluent English, Fluent Portuguese, Fluent German, Fluent Luxemburgish and know Spanish.</p>
						</li>
					</ul>
				</div>
			</div>
  );
};

export default Skills;
