import React from 'react';
import HeaderDetail from './common/HeaderDetail';

const NotFound = (props) => {
  return (
    <div>
      <HeaderDetail />
      <div className="container 404">
        <div className="headline">
          <div className="headline-container">
            <h2>404 Page Not Found</h2>
          </div>
        </div>
      </div>
    </div>
  );
}

export default NotFound;
