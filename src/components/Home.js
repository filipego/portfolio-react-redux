import React, { Component } from 'react';
import Header from './common/Header';
import Headline from './Headline';
import Portfolio from '../containers/Portfolio';
import About from './About';
import Skills from './Skills';


class Content extends Component {

  render() {
    return (
      <div>
        <Header />
        <Headline />
        <Portfolio/>
        <About />
        <Skills />
      </div>
    );
  }
}

export default Content;
